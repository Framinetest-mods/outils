#!/bin/bash

cd ..
for dir in ./*/

do
  cd ${dir}
  echo "---"
  echo "Depot ${dir%*/} : " && git status
  cd ..
done
