#! /bin/bash

me=$(basename "${BASH_SOURCE[0]}");

mkdir -p locale;
echo "Generating template..." >&2;
find . -iname "*.lua" | xargs xgettext --from-code=UTF-8 \
		--directory=. \
		--language=Lua \
		--sort-by-file \
		--keyword=S \
		--keyword=NS:1,2 \
		--keyword=N_ \
		--keyword=F \
		--add-comments='Translators:' \
		--add-location=file \
		-o locale/template.pot \
		|| exit;

find locale -name '*.po' -type f | while read -r file; do
	echo "Updating $file..." >&2;
	msgmerge --update "$file" locale/template.pot;
done

echo "DONE!" >&2;
