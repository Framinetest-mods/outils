# Outils

Au vu du nombre important de dépôts, voici quelques petits scripts d'aide.

## clonage_initial.sh
Doit être lancé à partir du dossier 'outils'.
Comme son nom l'indique, ce script clone tous les dépôts des mods.
Il ajoute aussi une source distante 'remote' correspondant à la branche 'upstream'.
Enfin, il récupère les infos de cette source.

## clonage_initial_https.sh
Idem ci-dessus mais avec des liens de clonage en https donc utilisables par tous.

## fetch_upstream.sh
Doit être lancé à partir du dossier 'outils'.
Un simple 'git fetch upstream' mais sur tous les dépôts en une seule fois.

## maj_depots.sh
Doit être lancé à partir du dossier 'outils'.
Un simple 'git pull' mais sur tous les dépôts en une seule fois.

## statut.sh
Doit être lancé à partir du dossier 'outils'.
Un simple 'git status' mais sur tous les dépôts en une seule fois.

## xgettext_dir.sh
Doit être lancé à partir du dossier de base du mod (celui qui contient le dossier 'locale').
Lance la recherche des chaines à traduire dans des fichiers lua (cf keyword).
Recrée le fichier template.pot s'il existe sinon le crée.
À partir de ce template, met à jour les fichiers .po existants.

**Ne pas utiliser sur les modpacks : 3d_armor, homedecor_modpack, plantlife_modpack**
Ils contiennent déjà un outil spécifique (cf tools/updatepo.sh) dans le mod du pack qui contient les traductions (respectivement 3d_armor, homedecor_i18n & plantlife_i18n).

## depots.txt
Ce fichier contient la liste de tous les dépôts du groupe Framinetest-mods (sauf 'outils').
Il contient également les adresses des dépôts 'upstream'.

## depots_https.txt
Idem depots.txt mais avec des liens https pour clonage simple.
