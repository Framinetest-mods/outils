#!/bin/bash

file='./outils/depots.txt'

cd ..

while read -r folder remote upstream; do
	git clone "$remote" "$folder"
	cd "$folder"
	git status >/dev/null 2>&1
	# check if exit status of above was 0, indicating we're in a git repo
	if [ $(echo $?) -eq 0 ] && [ "$upstream" != "" ]
	then
		echo "Ajoute la branche upstream pour $folder..."
		git remote add upstream "$upstream"
		echo "Récupère les infos de la branche upstream pour $folder..."
		git fetch upstream
	fi
	cd ..
done < "$file"
